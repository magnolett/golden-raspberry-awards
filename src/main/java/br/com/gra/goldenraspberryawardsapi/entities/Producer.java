package br.com.gra.goldenraspberryawardsapi.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "PRODUCER")
@AllArgsConstructor
@NoArgsConstructor
public class Producer {

    @Id
    @Column(name = "ID_PRODUCER")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", length = 50, nullable = false)
    private String name;

    @OneToMany(mappedBy = "producer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MovieProducer> movies = new ArrayList<>();

    public Producer(String name) {
        this.name = name;
    }

}
