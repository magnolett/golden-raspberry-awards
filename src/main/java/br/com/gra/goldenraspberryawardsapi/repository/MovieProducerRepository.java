package br.com.gra.goldenraspberryawardsapi.repository;

import java.util.List;

import br.com.gra.goldenraspberryawardsapi.entities.MovieProducer;
import br.com.gra.goldenraspberryawardsapi.entities.MovieProducerId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieProducerRepository extends JpaRepository<MovieProducer, MovieProducerId> {

    @Query(value = "select mp from MovieProducer as mp join mp.movie as movie join mp.producer as producer "
            + "where movie.winner = true order by producer.id, movie.year")
    List<MovieProducer> findByMovieWinnerOrderByProducerId(Boolean isWinner);

}
