package br.com.gra.goldenraspberryawardsapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProducerMinMaxPrizesDTO {

    private List<ProducerPrizesDTO> min = new ArrayList<>();

    private List<ProducerPrizesDTO> max = new ArrayList<>();

    public void addMin(ProducerPrizesDTO min) {
        this.getMin().add(min);
    }

    public void addMax(ProducerPrizesDTO max) {
        this.getMax().add(max);
    }
}

