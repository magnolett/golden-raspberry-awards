package br.com.gra.goldenraspberryawardsapi.repository;

import java.util.List;

import br.com.gra.goldenraspberryawardsapi.dto.StudioWinDTO;
import br.com.gra.goldenraspberryawardsapi.entities.Studio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudioRepository extends JpaRepository<Studio, Long> {

    Studio findByName(String name);

    @Query(value = "select new br.com.gra.goldenraspberryawardsapi.dto.StudioWinDTO(studio.name, count(movie.winner)) "
            + "from MovieStudio as ms join ms.movie as movie join ms.studio as studio "
            + "where movie.winner=true group by studio.name order by 2 desc")
    List<StudioWinDTO> findByWinners();

}
