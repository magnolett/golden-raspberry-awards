package br.com.gra.goldenraspberryawardsapi.entities;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class MovieStudioId implements Serializable {

    private static final long serialVersionUID = -5522529450406784648L;

    private Long idMovie;

    private Long idStudio;
}