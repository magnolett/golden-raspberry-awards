package br.com.gra.goldenraspberryawardsapi.repository;

import java.util.List;

import br.com.gra.goldenraspberryawardsapi.dto.YearWinnerMovieDTO;
import br.com.gra.goldenraspberryawardsapi.entities.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    List<Movie> findByYear(Integer year);

    @Query(value = "select new br.com.gra.goldenraspberryawardsapi.dto.YearWinnerMovieDTO(movie.year, count(movie.winner)) "
            + "from Movie as movie where movie.winner=true group by movie.year having count(movie.winner) > 1")
    List<YearWinnerMovieDTO> findYearsWithModeThanOneWinner();

}
