package br.com.gra.goldenraspberryawardsapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.gra.goldenraspberryawardsapi.dto.MovieDTO;
import br.com.gra.goldenraspberryawardsapi.dto.YearWinnerDTO;
import br.com.gra.goldenraspberryawardsapi.dto.YearWinnerMovieDTO;
import br.com.gra.goldenraspberryawardsapi.entities.Movie;
import br.com.gra.goldenraspberryawardsapi.exception.BadRequestException;
import br.com.gra.goldenraspberryawardsapi.exception.ResourceNotFoundException;
import br.com.gra.goldenraspberryawardsapi.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MovieService {

    Logger logger = LoggerFactory.getLogger(MovieService.class);

    private final MovieRepository movieRepository;

    public List<Movie> getMoviesFromAYear(Integer year) {
        return movieRepository.findByYear(year);
    }

    public List<MovieDTO> getMoviesByYear(Integer year) {
        List<Movie> movies = movieRepository.findByYear(year);

        if (movies == null || movies.isEmpty()) {
            return new ArrayList<>();
        }

        List<MovieDTO> moviesDto = new ArrayList<>();
        for (Movie m : movies) {
            moviesDto.add(new MovieDTO(m));
        }

        return moviesDto;
    }

    public YearWinnerDTO getYearsWithMoreThanOneWinners() {
        List<YearWinnerMovieDTO> years = movieRepository.findYearsWithModeThanOneWinner();
        if (years == null || years.isEmpty()) {
            return new YearWinnerDTO();
        }
        return new YearWinnerDTO(years);
    }

    public void remove(Long id) {
        Optional<Movie> optional = movieRepository.findById(id);

        if (!optional.isPresent())
            throw new ResourceNotFoundException();

        Movie movie = optional.get();
        if (movie.getWinner())
            throw new BadRequestException();

        movieRepository.delete(movie);
    }

}
