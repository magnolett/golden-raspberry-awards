## Projeto Golden Raspberry Awards ##

# Tecnologias usadas:

- SpringBoot 3.0.1
- Java 17
- OpenAPI 2.0.2
- IntelliJ

O restante pode ser verificado no arquivo build.gradle

# Requisitos

- Java 17
- Uma IDE de preferência

# Inicialização

Para inicializar o projeto, é necessário fazer clone do repositório e executá-lo pela classe GoldenRaspberryAwardsApiApplication

# Configurações

O projeto possui um contextPath inserido no arquivo application.yml, por padrão é localhost:8080/**gra**
No mesmo arquivo pode ser conferidas as configurações do banco H2 utilizado no desenvolvimento do mesmo.

# Endpoints

Com o uso do OpenAPI, toda a documentação dos endpoints está inserida no path /swagger-ui/index.html
O caminho padrão é localhost:8080/gra/swagger-ui/index.html

# Testes

Para rodar os testes basta acessá-los e rodar com o JUnit.
