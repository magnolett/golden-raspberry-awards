package br.com.gra.goldenraspberryawardsapi.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "MOVIE")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Movie {

    @Id
    @Column(name = "ID_MOVIE")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "LAUNCH_YEAR", nullable = false)
    private Integer year;

    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "IS_WINNER", nullable = false)
    private Boolean winner;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<MovieStudio> studios = new HashSet<>();

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<MovieProducer> producers = new HashSet<>();

    public static Movie buildMovie(Integer year, String title, String winner) {
        return Movie.builder().year(year).title(title).winner("yes".equalsIgnoreCase(winner)).build();
    }
}