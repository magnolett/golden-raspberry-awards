package br.com.gra.goldenraspberryawardsapi.service;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

import br.com.gra.goldenraspberryawardsapi.dto.ProducerMinMaxPrizesDTO;
import br.com.gra.goldenraspberryawardsapi.dto.ProducerPrizesDTO;
import br.com.gra.goldenraspberryawardsapi.entities.Movie;
import br.com.gra.goldenraspberryawardsapi.entities.MovieProducer;
import br.com.gra.goldenraspberryawardsapi.entities.Producer;
import br.com.gra.goldenraspberryawardsapi.repository.MovieProducerRepository;
import br.com.gra.goldenraspberryawardsapi.repository.ProducerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.sound.sampled.Port;

@Service
public class ProducerService {

    Logger logger = LoggerFactory.getLogger(ProducerService.class);

    @Autowired
    private ProducerRepository producerRepository;

    @Autowired
    private MovieProducerRepository movieProducerRepository;

    public void saveProducers(Movie movie, String producers) {
        for (String strProducer : producers.split(",|\\ and ")) {
            Producer producer = new Producer(strProducer.trim());

            Example<Producer> example = Example.of(producer);

            if (producerRepository.exists(example)) {
                producer = producerRepository.findByName(strProducer.trim());
            } else {
                producer = producerRepository.save(producer);
            }

            movieProducerRepository.save(MovieProducer.buildMovieProducer(movie, producer));
        }
    }

    public ProducerMinMaxPrizesDTO getMaxAndMinPrizes() {
        List<MovieProducer> mpList = movieProducerRepository.findByMovieWinnerOrderByProducerId(true);

        List<ProducerPrizesDTO> minInterval = findMinInterval(mpList);
        List<ProducerPrizesDTO> maxInterval = findMaxInterval(mpList);

        ProducerMinMaxPrizesDTO dto = new ProducerMinMaxPrizesDTO();

        dto.setMin(minInterval);
        dto.setMax(maxInterval);
        return dto;
    }

    private List<ProducerPrizesDTO> findInterval(List<MovieProducer> mpList, boolean isMax) {
        List<ProducerPrizesDTO> intervals = new ArrayList<>();
        int interval = isMax ? Integer.MIN_VALUE : Integer.MAX_VALUE;

        for (int i = 0; i < mpList.size() - 1; i++) {

            MovieProducer mpi = mpList.get(i);
            MovieProducer mpj = mpList.get(i + 1);

            if (mpi.getProducer().equals(mpj.getProducer())) {
                Integer currentInterval = Math.abs(mpj.getMovie().getYear() - mpi.getMovie().getYear());

                if (isMax && currentInterval > interval || !isMax && currentInterval < interval) {
                    interval = currentInterval;
                    intervals.clear();
                    intervals.add(new ProducerPrizesDTO(mpi.getProducer().getName(), currentInterval, mpi.getMovie().getYear(), mpj.getMovie().getYear()));
                } else if (currentInterval == interval) {
                    intervals.add(new ProducerPrizesDTO(mpi.getProducer().getName(), currentInterval, mpi.getMovie().getYear(), mpj.getMovie().getYear()));
                }
            }
        }

        return intervals;
    }

    private List<ProducerPrizesDTO> findMaxInterval(List<MovieProducer> mpList) {
        return findInterval(mpList, true);
    }

    private List<ProducerPrizesDTO> findMinInterval(List<MovieProducer> mpList) {
        return findInterval(mpList, false);
    }
}


