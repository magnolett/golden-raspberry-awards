package br.com.gra.goldenraspberryawardsapi.repository;

import br.com.gra.goldenraspberryawardsapi.entities.MovieStudio;
import br.com.gra.goldenraspberryawardsapi.entities.MovieStudioId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieStudioRepository extends JpaRepository<MovieStudio, MovieStudioId> {

}
