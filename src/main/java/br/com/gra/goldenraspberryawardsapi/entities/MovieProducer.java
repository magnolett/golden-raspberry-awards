package br.com.gra.goldenraspberryawardsapi.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@Entity
@Table(name = "MOVIE_PRODUCER")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MovieProducer {

    @EmbeddedId
    private MovieProducerId id;

    @ManyToOne
    @MapsId("idMovie")
    private Movie movie;

    @ManyToOne
    @MapsId("idProducer")
    private Producer producer;

    public static MovieProducer buildMovieProducer(Movie movie, Producer producer) {
        return MovieProducer.builder().id(new MovieProducerId(movie.getId(), producer.getId())).movie(movie).producer(producer).build();
    }
}
