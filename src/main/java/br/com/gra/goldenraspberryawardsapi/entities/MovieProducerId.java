package br.com.gra.goldenraspberryawardsapi.entities;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class MovieProducerId implements Serializable {

    private static final long serialVersionUID = -5332423598071950748L;

    private Long idMovie;

    private Long idProducer;
}
