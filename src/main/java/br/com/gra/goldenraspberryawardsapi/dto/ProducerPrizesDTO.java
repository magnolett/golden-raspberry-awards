package br.com.gra.goldenraspberryawardsapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProducerPrizesDTO {

    private String producer;

    private Integer interval;

    private Integer previousWin;

    private Integer followingWin;
}
