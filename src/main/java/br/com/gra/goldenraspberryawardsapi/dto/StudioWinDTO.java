package br.com.gra.goldenraspberryawardsapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudioWinDTO {

    private String name;

    private Long winCount;
}
