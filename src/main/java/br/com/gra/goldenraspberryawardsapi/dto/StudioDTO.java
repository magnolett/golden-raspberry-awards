package br.com.gra.goldenraspberryawardsapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudioDTO {

    private List<StudioWinDTO> studios;

}
