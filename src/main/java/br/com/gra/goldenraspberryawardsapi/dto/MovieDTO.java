package br.com.gra.goldenraspberryawardsapi.dto;

import br.com.gra.goldenraspberryawardsapi.entities.Movie;
import br.com.gra.goldenraspberryawardsapi.entities.MovieProducer;
import br.com.gra.goldenraspberryawardsapi.entities.MovieStudio;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieDTO {

    private Long id;

    private Integer year;

    private String title;

    private List<String> studios = new ArrayList<>();

    private List<String> producers = new ArrayList<>();

    private Boolean winner;

    public MovieDTO(Movie movie) {
        this.id = movie.getId();
        this.year = movie.getYear();
        this.title = movie.getTitle();
        this.winner = movie.getWinner();

        for (MovieStudio ms : movie.getStudios()) {
            this.studios.add(ms.getStudio().getName());
        }

        for (MovieProducer mp : movie.getProducers()) {
            this.producers.add(mp.getProducer().getName());
        }
    }
}
