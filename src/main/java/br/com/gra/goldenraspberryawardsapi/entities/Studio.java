package br.com.gra.goldenraspberryawardsapi.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "STUDIO")
@AllArgsConstructor
@NoArgsConstructor
public class Studio {

    @Id
    @Column(name = "ID_STUDIO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", length = 50, unique = true)
    private String name;

    @OneToMany(mappedBy = "studio", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MovieStudio> movies = new ArrayList<>();

    public Studio(String name) {
        this.name = name;
    }

}
