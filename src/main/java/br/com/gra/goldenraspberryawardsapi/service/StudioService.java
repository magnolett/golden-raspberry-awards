package br.com.gra.goldenraspberryawardsapi.service;

import br.com.gra.goldenraspberryawardsapi.dto.StudioDTO;
import br.com.gra.goldenraspberryawardsapi.entities.Movie;
import br.com.gra.goldenraspberryawardsapi.entities.MovieStudio;
import br.com.gra.goldenraspberryawardsapi.entities.Studio;
import br.com.gra.goldenraspberryawardsapi.repository.MovieStudioRepository;
import br.com.gra.goldenraspberryawardsapi.repository.StudioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudioService {

    private final StudioRepository studioRepository;

    private final MovieStudioRepository movieStudioRepository;

    public void saveStudios(Movie movie, String studios) {
        for (String strStudio : studios.split(",|\\ and ")) {
            Studio studio = new Studio(strStudio.trim());

            Example<Studio> example = Example.of(studio);

            if (studioRepository.exists(example)) {
                studio = studioRepository.findByName(strStudio.trim());
            } else {
                studio = studioRepository.save(studio);
            }


            movieStudioRepository.save(MovieStudio.buildMovieStudio(movie, studio));
        }
    }

    public StudioDTO getGreatestWinners() {
        return new StudioDTO(studioRepository.findByWinners());
    }

}