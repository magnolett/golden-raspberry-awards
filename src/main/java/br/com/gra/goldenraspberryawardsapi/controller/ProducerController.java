package br.com.gra.goldenraspberryawardsapi.controller;

import br.com.gra.goldenraspberryawardsapi.dto.ProducerMinMaxPrizesDTO;
import br.com.gra.goldenraspberryawardsapi.service.ProducerService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/producer")
@RequiredArgsConstructor
public class ProducerController {

    Logger logger = LoggerFactory.getLogger(ProducerController.class);

    private final ProducerService producerService;

    @GetMapping("/interval-prizes")
    public ResponseEntity<ProducerMinMaxPrizesDTO> getMaxAndMinPrizes() {
        ProducerMinMaxPrizesDTO dto = producerService.getMaxAndMinPrizes();

        HttpStatus status = HttpStatus.OK;
        if (dto.getMax().isEmpty() && dto.getMin().isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        return new ResponseEntity<>(dto, status);
    }

}
