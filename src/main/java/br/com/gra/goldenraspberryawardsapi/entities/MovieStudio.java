package br.com.gra.goldenraspberryawardsapi.entities;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "MOVIE_STUDIO")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MovieStudio {

    @EmbeddedId
    private MovieStudioId id;

    @ManyToOne
    @MapsId("idMovie")
    private Movie movie;

    @ManyToOne
    @MapsId("idStudio")
    private Studio studio;

    public static MovieStudio buildMovieStudio(Movie movie, Studio studio) {
        return MovieStudio.builder().id(new MovieStudioId(movie.getId(), studio.getId())).movie(movie).studio(studio).build();
    }
}
