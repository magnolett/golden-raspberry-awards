package br.com.gra.goldenraspberryawardsapi.repository;

import br.com.gra.goldenraspberryawardsapi.entities.Producer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProducerRepository extends JpaRepository<Producer, Long> {

    Producer findByName(String name);

}
