package br.com.gra.goldenraspberryawardsapi.controller;

import java.util.List;

import br.com.gra.goldenraspberryawardsapi.dto.MovieDTO;
import br.com.gra.goldenraspberryawardsapi.dto.YearWinnerDTO;
import br.com.gra.goldenraspberryawardsapi.service.MovieService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/movie")
@RequiredArgsConstructor
public class MovieController {

    private final MovieService movieService;

    @GetMapping("/{year}")
    public ResponseEntity<List<MovieDTO>> getMovies(@PathVariable(name = "year") Integer year) {
        List<MovieDTO> movies = movieService.getMoviesByYear(year);

        HttpStatus status = HttpStatus.OK;
        if (movies.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        return new ResponseEntity<>(movies, status);
    }

    @GetMapping("/years")
    public ResponseEntity<YearWinnerDTO> getYearsWithMoreThanOneWinners() {
        YearWinnerDTO dto = movieService.getYearsWithMoreThanOneWinners();

        HttpStatus status = HttpStatus.OK;
        if (dto.getYears().isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        return new ResponseEntity<>(dto, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeMovie(@PathVariable(name = "id") Long id) {
        movieService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
