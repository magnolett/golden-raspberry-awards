package br.com.gra.goldenraspberryawardsapi;

import br.com.gra.goldenraspberryawardsapi.entities.Movie;
import br.com.gra.goldenraspberryawardsapi.repository.MovieRepository;
import br.com.gra.goldenraspberryawardsapi.service.ProducerService;
import br.com.gra.goldenraspberryawardsapi.service.StudioService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

@SpringBootApplication
@RequiredArgsConstructor
public class GoldenRaspberryAwardsApiApplication {

    private final MovieRepository movieRepository;

    private final StudioService studioService;

    private final ProducerService producerService;

    public static void main(String[] args) {
        SpringApplication.run(GoldenRaspberryAwardsApiApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext appContext) {
        return args -> {

            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            InputStream in = cl.getResourceAsStream("movielist.csv");
            Reader reader = new InputStreamReader(in);
            Iterable<CSVRecord> records = CSVFormat.RFC4180
                    .withDelimiter(';')
                    .withHeader("year", "title", "studios", "producers", "winner")
                    .parse(reader);

            for (CSVRecord record : records) {
                if (record.getRecordNumber() == 1) {
                    continue;
                }

                String winner = record.get("winner");
                Movie movie = movieRepository.save(Movie.buildMovie(Integer.valueOf(record.get("year")), record.get("title"), winner));

                String studios = record.get("studios");
                studioService.saveStudios(movie, studios);

                String producers = record.get("producers");
                producerService.saveProducers(movie, producers);
            }

        };
    }
}
