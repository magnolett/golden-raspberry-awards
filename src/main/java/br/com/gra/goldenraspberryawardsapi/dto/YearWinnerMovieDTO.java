package br.com.gra.goldenraspberryawardsapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class YearWinnerMovieDTO {

    private Integer year;

    private Long winnerCount;
}
